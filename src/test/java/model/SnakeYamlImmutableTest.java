package model;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.CustomClassLoaderConstructor;

/**
 * @author: yerlibilgin
 * @date: 15/08/16.
 */
public class SnakeYamlImmutableTest {

  @org.junit.Test
  public void testLoadYamlFailing() throws Exception {
    Yaml yaml = new Yaml(new CustomClassLoaderConstructor(this.getClass().getClassLoader()));
    try {
      yaml.load(this.getClass().getResourceAsStream("/data.yml"));
    }catch (Exception ex){
      ex.printStackTrace();
      return;
    }

    throw new Exception("Oops! It should have failed but it didn't.");
  }

  @org.junit.Test
  public void testLoadYamlWorking() throws Exception {
    Yaml yaml = new Yaml(new CustomClassLoaderConstructor(this.getClass().getClassLoader()));
    yaml.load(this.getClass().getResourceAsStream("/data2.yml"));
  }
}
